import random
import uuid


from graphviz import Digraph

def collatz(num: int):
    if num % 2 == 0:
        return int(num / 2)
    return 3 * num + 1

def create_collatz_tagged_list(_try):
    # ["uid+4","uid+2","uid+1"]
    collatz_list = []
    seen_4_before = False
    while _try != 4 or not seen_4_before:
        if _try == 4:
            seen_4_before = True
        tag = _try if _try in [4,2,1] else str(uuid.uuid4())
        collatz_list.append(f'{tag}+{str(_try)}')
        _try = collatz(_try)
    return collatz_list

def nodes(collatz_lists):
    """
    given: [["uuid+4","uuid+2","uuid+1"], ...]
    outputs: [[[uuid, "4"], [uuid, "2"], [uuid, "1"]], ...]
    """
    tagged_edges = []
    for col_list in collatz_lists:
        tagged_graph = []
        for node in col_list:
            uid, number = node.split("+")
            tagged_graph.append([uid, number])
        tagged_edges.append(tagged_graph)
    return tagged_edges

def create_nodes_network(dot, tagged_graph):
    for tagged_edges in tagged_graph:
        for node in tagged_edges:
            dot.node(*node)

def create_self_name_nodes_network(dot, graphs):
    for graph in graphs:
        for node in graph:
            dot.node(node, node)

def create_edges_with_tagged_nodes(dot, tagged_graphs):
    """
    Given: [[[uuid, "4"], [uuid, "2"], [uuid, "1"]], ...]
    Output: 
    """
    for tagged_graph in tagged_graphs:
        i = 0
        while i < len(tagged_graph) - 1:
            dot.edge(tagged_graph[i][0], tagged_graph[i+1][0])
            i = i+1

def create_edges(dot, tagged_graphs):
    for tagged_graph in tagged_graphs:
        i = 0
        while i < len(tagged_graph) - 1:
            dot.edge(tagged_graph[i], tagged_graph[i+1])
            i = i+1

def create_collatz_list(_try):
    # ["4","2","1"]
    collatz_list = []
    seen_4_before = False
    while _try != 4 or not seen_4_before:
        if _try == 4:
            seen_4_before = True
        collatz_list.append(str(_try))
        _try = collatz(_try)
    return collatz_list

def full_collatz(tries):
    dot = Digraph(format="png", filename="full_collatz")
    collatz_tagged_lists = list()
    for i in tries:
        collatz_tagged_lists.append(create_collatz_tagged_list(i))
    tagged_edges = nodes(collatz_tagged_lists)
    create_nodes_network(dot, tagged_edges)
    create_edges_with_tagged_nodes(dot, tagged_edges)
    dot.render()    

def collatz_merged(tries):
    dot = Digraph(format="png", filename="merged")
    
    collatz_lists = list()
    for i in tries:
        collatz_lists.append(create_collatz_list(i))
    create_self_name_nodes_network(dot, collatz_lists)
    create_edges(dot, collatz_lists)
    dot.render()

def main():
    tries = [9,43,33,39,45,15]     
    full_collatz(tries)
    collatz_merged(tries)

main()
